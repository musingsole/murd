from unittest import TestCase
import os
import json
from murd import Murd


class TestMurd(TestCase):
    def test_init(self):
        murd = Murd()
        self.assertEqual(murd.matrix, r'{}')
        self.assertDictEqual(murd.murds, {murd.default_murd_name: murd})

        expected = '{"GROUP": "foo", "SORT": "_", "DATA": "bar"}'
        with open("test.json", "w") as f:
            f.write(expected)
        murd = Murd("test.json")
        self.assertEqual(expected, murd.matrix)
        os.remove("test.json")

    def test_update(self):
        murd = Murd()
        murd.murds['1'] = Murd()
        murd.update([{"GROUP": "TEST", "SORT": "all", "Data": 1}], murd.murds)
        murd.update([{"GROUP": "TEST", "SORT": "0", "Data": 2}])
        murd.update([{"GROUP": "TEST", "SORT": "1", "Data": 3}], '1')

        self.assertEqual(
            murd.murds['0'].matrix,
            json.dumps(Murd.prime_ms([
                {"GROUP": "TEST", "SORT": "all", "Data": 1},
                {"GROUP": "TEST", "SORT": "0", "Data": 2}
            ]))
        )
        self.assertEqual(
            murd.murds['1'].matrix,
            json.dumps(Murd.prime_ms([
                {"GROUP": "TEST", "SORT": "all", "Data": 1},
                {"GROUP": "TEST", "SORT": "1", "Data": 3}]))
        )

    def test_delete(self):
        murd = Murd()
        murd.matrix = json.dumps(Murd.prime_ms([
            {"GROUP": "TEST", "SORT": "all", "Data": 1},
            {"GROUP": "TEST3", "SORT": "1", "Data": 3},
            {"GROUP": "TEST2", "SORT": "1", "Data": 3},
            {"GROUP": "TEST2", "SORT": "3", "Data": 3}
        ]))
        murd.delete([{"GROUP": "TEST3", "SORT": "1", "Data": 3},
                     {"GROUP": "TEST2", "SORT": "1", "Data": 3}])
        self.assertEqual(
            json.dumps(Murd.prime_ms([
                {"GROUP": "TEST", "SORT": "all", "Data": 1},
                {"GROUP": "TEST2", "SORT": "3", "Data": 3}
            ])),
            murd.matrix
        )

    def test_read1(self):
        murd = Murd()
        murd.matrix = json.dumps(Murd.prime_ms([
            {"GROUP": "TEST", "SORT": "all", "Data": 1},
            {"GROUP": "TEST3", "SORT": "1", "Data": 3},
            {"GROUP": "TEST2", "SORT": "1", "Data": 3},
            {"GROUP": "TEST2", "SORT": "3", "Data": 3}
        ]))
        murd.murds['1'] = Murd()
        murd.murds['1'].default_murd_name = '1'
        murd.murds['1'].matrix = json.dumps(Murd.prime_ms([
            {"GROUP": "TEST", "SORT": "all", "Data": 5},
            {"GROUP": "TEST3", "SORT": "5", "Data": 3}
        ]))

        test_1 = [{'Data': 1, 'GROUP': 'TEST', 'SORT': 'all'},
                  {'Data': 5, 'GROUP': 'TEST', 'SORT': 'all'}]
        results = murd.read(partition="TEST", sort="all")
        self.assertListEqual(results, test_1)

    def test_read2(self):
        murd = Murd()
        murd.matrix = json.dumps(Murd.prime_ms([
            {"GROUP": "TEST", "SORT": "all", "Data": 1},
            {"GROUP": "TEST3", "SORT": "1", "Data": 3},
            {"GROUP": "TEST2", "SORT": "1", "Data": 3},
            {"GROUP": "TEST2", "SORT": "3", "Data": 3}
        ]))
        murd.murds['1'] = Murd(default_murd_name='1')
        murd.murds['1'].matrix = json.dumps(Murd.prime_ms([
            {"GROUP": "TEST", "SORT": "all", "Data": 5},
            {"GROUP": "TEST3", "SORT": "5", "Data": 3}
        ]))
        test_2 = [{'Data': 3, 'GROUP': 'TEST2', 'SORT': '3'}]
        results = murd.read(partition="TEST2", min_sort="2")
        self.assertListEqual(results, test_2)

    def test_read_sort(self):
        murd = Murd()
        murd.matrix = json.dumps(Murd.prime_ms([
            {"GROUP": "TEST", "SORT": "1", "Data": 1},
            {"GROUP": "TEST", "SORT": "2", "Data": 3},
            {"GROUP": "TEST", "SORT": "3", "Data": 3},
            {"GROUP": "TEST", "SORT": "4", "Data": 3}
        ]))
        test_descending = [
            {'Data': 3, 'GROUP': 'TEST', 'SORT': '4'},
            {'Data': 3, 'GROUP': 'TEST', 'SORT': '3'},
            {'Data': 3, 'GROUP': 'TEST', 'SORT': '2'},
            {'Data': 1, 'GROUP': 'TEST', 'SORT': '1'}]
        test_ascending = [
            {'Data': 1, 'GROUP': 'TEST', 'SORT': '1'},
            {'Data': 3, 'GROUP': 'TEST', 'SORT': '2'},
            {'Data': 3, 'GROUP': 'TEST', 'SORT': '3'},
            {'Data': 3, 'GROUP': 'TEST', 'SORT': '4'}]
        self.assertListEqual(test_descending, murd.read(partition="TEST"))
        self.assertListEqual(test_ascending, murd.read(partition="TEST", ascending_order=True))
